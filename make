#! /bin/bash

# get root & src path
root="$(cd "$(dirname $0)" && pwd)"
src=$root/src


# include script
. "$src/scripts/.script.sh"


# export path
#if [ -z $1 ]; then
#    read -p "Export path: " path
#else
#    out="$(cut -d':' -f1 <<<"$1")"
#fi

out="$root/out"



# create directory
rm -rf $out/* && rm -rf $out/.* 2> /dev/null
mkdir -p $out
mkdir -p $out/conf



# copy doc
read -s -p "Include doc? (Y/N)" yn
echo ""

if [[ "$yn" =~ [yY] ]]; then
    cp -rf $root/doc $out
fi



# root
mkdir -p $out/vagrant

find $src -maxdepth 1 -type f -exec cp "{}" $out \;
cp -f $src/vagrant/Vagrantfile $out
cp -f $src/vagrant/host $out
cp -f $src/vagrant/key $out



# vagrant
cp -f $src/vagrant/Vagrantsettings.rb $out/vagrant
find $src/vagrant/conf/* -type f -not -iname '*.sample' -exec cp {} $out/conf \;



# scripts
mkdir -p $out/scripts/conf
mkdir -p $out/scripts/doc
mkdir -p $out/scripts/apps
cp -rf $src/scripts/data $out/scripts
cp $src/scripts/* $src/scripts/.* $out/scripts 2> /dev/null
rm $out/scripts/link

. "$src/scripts/packages"
for package in ${packages[@]};do
    cp -rf $src/scripts/apps/$version/$package $out/scripts/apps
    cp -rf $out/scripts/apps/$package/conf/* $out/scripts/conf 2> /dev/null
    rm -rf $out/scripts/apps/$package/conf 2> /dev/null
    cp -rf $out/scripts/apps/$package/doc/* $out/scripts/doc 2> /dev/null
    rm -rf $out/scripts/apps/$package/doc 2> /dev/null
done




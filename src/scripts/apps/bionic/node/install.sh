#! /bin/bash
# from "https://github.com/nodesource/distributions"

print_title "Node 10.x & npm: Installation"

print_start "getting repositories.."
#sudo curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
#sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt update -y -qq

print_start "installing.."
sudo apt install nodejs -y
sudo apt install build-essential -y

print_start "cleaning up.."
sudo apt autoremove -y -qq
print_end "install finished.."


# to update
# sudo curl -sL https://deb.nodesource.com/setup_{version_no}.x | sudo -E bash -
# sudo apt install nodejs
# sudo npm install -g npm


# install NVM to MAC
# curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
# add this to bashrc
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
* **Update configuration and sites* 
    * run `bash setup lamp` every time a configuration is changed. It will copy configurations into the designated paths.

* *Change PHP version for CLI*
    ```
    sudo update-alternatives --config php
    ```  
* *Available executables*
    * `xdebug [start|stop]`: Start or Stop PHP XDebug service    

* *Available makes*
    * `bash make ssl [domain]`: Create a ssl cert & key to /etc/ssl
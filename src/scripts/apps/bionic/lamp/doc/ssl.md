**1. Create rootCA**
```
sudo su
mkdir /etc/ssl/root && cd /etc/ssl/root
openssl genrsa -des3 -out rootCA.key 2048
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 500 -out rootCA.pem
```

**2. Trust the Root CA on your local machine**

>MAC
1. copy the `rootCA.pem` to `/scripts/data` for shared access
2. open `Keychain Access` app
3. `File > Import Items` and select `rootCA.pem` to import the key
4. goto `Certificates` category and double click `Local Certificate` you just imported
5. goto `trust section` and change `when using this certificate:` to `Trust Always`
    
>Windows
1. copy the `rootCA.pem` to `/scripts/data` for shared access
2. `Start > Run` and enter `mmc` to open Microsoft Management Console
3. goto `File > Add/Remove Snap-in` then click `Certificates` and select `Add`
4. select `Computer Account` and click `Next`
5. select `Local Computer` and click `Finish`
6. click `OK` to go back to main MMC console window
7. double click `Certificates (local computer)` to expand its view
8. right click `Certificates under Trusted Root Certification Authorities` and select `All Tasks` then `Import`
9. complete the wizard to import the certificate. Browse to locate `rootCA.pem` file to import
10. select `Place all certificates in the following store` and select `Trusted Root Certification Authorities store`. Click `Next` then click `Finish`

**3. Issue Certificate for a local domain**
```
cp /scripts/conf/ssl/sample.v3.ext domain.v3.ext
### edit the v3 and change the DNS.1 value
openssl req -new -sha256 -nodes -out domain.csr -newkey rsa:2048 -keyout domain.key
openssl x509 -req -in domain.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out domain.crt -days 500 -sha256 -extfile domain.v3.ext

mkdir /etc/ssl/domain
mv domain.* /etc/ssl/domain 
```







https://reactpaths.com/how-to-get-https-working-in-localhost-development-environment-f17de34af046
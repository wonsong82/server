#! /bin/bash
print_title "Composer: Configuration"
sudo cp -f $app/inc/config.json ~/.config/composer/
sudo chown $USER:$USER ~/.config/composer/config.json
print_end "finished"

#! /bin/bash
print_title "Laravel Installer: Uninstall"

composer global remove laravel/installer

print_end "Laravel Installer removed.."

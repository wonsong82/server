1. **Host machine installation** 
    
    > Mac
    
    1. make a `~/Unison` folder
    
    2. copy `inc/Unison-v.OS.X.zip` into the `~/Unison` folder, unzip and delete the zip file and run `Unison.app` to install. Make sure to add a command line tool.
    
    3. install `homebrew` if you haven't already.
        ```
        $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        ```
    
    4. `$ sudo easy_install pip` to install pip
    
    5. `$ pip install --user macfsevents` to install MacFSEvents for fs-monitor
    
    6. `$ brew install autozimu/formulas/unison-fsmonitor` to install fs-monitor
    
    > Windows
    
    1. make a `~/Unison` folder
    
    2. copy `inc/unison-windows-v-text.zip` into the `~/Unison` folder, unzip it and delete the zip file
    
    3. add the Unison folder (Absolute Path) into System Variable
    
2. **Config**
    
    1. copy `conf/unison/sample.prf` into your machine's unison path
        * Mac: `~/Library/Application Support/Unison`
        * Windows: `~/.unison`
    
    2. rename `sample.prf` and update `root` paths
       
3. **Run unison**
    
    > Mac 
    ```
    $ unison profile_name -ui text
    ```
    
    > Windows
    ```
    $ unison profile_name
    ```

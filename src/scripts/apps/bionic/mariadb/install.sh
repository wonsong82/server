#! /bin/bash
print_title "MariaDB: Installation"

print_start "getting git repositories.."
sudo apt install software-properties-common -y
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://suro.ubaya.ac.id/mariadb/repo/10.3/ubuntu bionic main'
sudo apt update -y -qq

print_start "installing.."
sudo service mysql stop
sudo apt install mariadb-server -y

print_start "cleaning up.."
sudo apt autoremove -y -qq
print_end "install finished.."



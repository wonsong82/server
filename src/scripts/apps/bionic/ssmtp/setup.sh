#! /bin/bash
. "$config/ssmtp.conf"
print_title "SSMTP: Configuration"

sudo cp -f $app/inc/$provider.conf $config/ssmtp.conf.temp

if [ $provider == "mailtrap" ]; then
    search="ssmtp_auth_user"
    replace="$auth_user"
    sed -i "s~$search~$replace~g" $config/ssmtp.conf.temp

    search="ssmtp_auth_pass"
    replace="$auth_pass"
    sed -i "s~$search~$replace~g" $config/ssmtp.conf.temp
fi


sudo mv -f $config/ssmtp.conf.temp /etc/ssmtp/ssmtp.conf
sudo chown root:mail /etc/ssmtp/ssmtp.conf
sudo chmod 640 /etc/ssmtp/ssmtp.conf


print_end "finished"

#!/usr/bin/env bash
# set root dir
function getRoot {
    echo "$( cd "$( dirname $0 )" && pwd )"
}


# require $filepath
function require {
  if [ -e $1 ]; then
	  . $1
	else
	  print_error "cannot find $1"
	fi
}

# create temporary config to use mysql in command line
# must run end_mysql after use
function start_mysql {
  sudo rm -rf ~/.my.cnf
  sudo cat > ~/.my.cnf << EOF
[client]
user=root
host=localhost
password=$db_root_pass
EOF
sudo chmod 0600 ~/.my.cnf
}

# delete the temporary config
function end_mysql {
  sudo rm -rf ~/.my.cnf
}


#print header
function print_title {
  tput setaf 2;echo "";echo "";echo ""
  echo "------------------------------------------------"
  echo ">> $1 "
  echo "------------------------------------------------"
  echo "";tput sgr0

}

function print_start {
  tput setaf 2
  echo ""
  echo ">> $1"
  tput sgr0
}

function print_end {
  tput setaf 2
  echo ""
  echo ">> $1"
  echo ""
  tput sgr0
}

# print_error $msg
function print_error {
  tput setaf 1
  echo ">> error: $1"
  tput sgr0
}

function getOSType {
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        echo 'linux'
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        echo 'linux' # Mac OSX
    elif [[ "$OSTYPE" == "cygwin" ]]; then
        echo 'win' # POSIX compatibility layer and Linux environment emulation for Windows
    elif [[ "$OSTYPE" == "msys" ]]; then
        echo 'win' # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
    elif [[ "$OSTYPE" == "win32" ]]; then
        echo 'win' # I'm not sure this can happen.
    elif [[ "$OSTYPE" == "freebsd"* ]]; then
        echo 'linux' # ...
    else
        echo 'linux' # Unknown.
    fi
}
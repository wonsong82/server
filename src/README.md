#Installation Guide

### - Prerequisite -
>* PHPStorm or IntelliJ
>* Git Bash (for windows)
>* Virtual Box
>* Vagrant

### - Installation -
1. **Vagrant**
    1. Configure `conf/vagrant.json`
        * `name`: assign unique name for VirtualBox
        
        * `cpus`: auto will assign 1/2 of cores
        
        * `memory`: auto will assign 1/4 of memory
        
        * `folders`: add relative paths to nfs shared folders
            > `/scripts/data` folder is git ignored and can be used as data sharing folder  
            ```
            "folders": [
                {"from": "../", "to": "/shared/"
            ]
            ```
        * `binds`: binds owner & permission to shared folders
            ```
            "binds": [
                {"from": "/shared", "to": "/projects/", "user": "vagrant", "group": "www-data", "permissions": "u=rwx:g=rwx:o=rx"}
            ]
    
    2. Start the server
        ```
        $ vagrant up
        ```
    
    3. [Optional] Add your domains configured in `conf/vagrant.json` automatically to your host file.
        ```
        $ ./host
        ```
    
    4. [Optional] Copy your keys from host to guest so they can use the same keys. Also make vagrant accessible with the key.
        ```
        $ ./key
        
        ### when it prompt you to provide a password, use 'vagrant'
        
        ### now you can ssh to vagrant by
        $ ssh vagrant@192.168.XX.XX
        ```
        
2. **Server scripts**
    > For fresh installation, your server will include scripts in `/scripts`. please configure the necessary files in `/scripts/conf` before run the scripts
    
    1. To install packages listed on `packages`
        ```
        ### for all packages
        $ bash install
        
        ### for specific package(s)
        $ bash install $package1 $package2 ...
        ```
    
    2. To run designated setup & configuration tasks
        ```
        ### for all packages
        $ bash setup
        
        ### for specific package(s)
        $ bash setup $package1 $package2 ...
        ```
        
    3. To remove packages
        ```
        ### for all packages
        $ bash setup
        
        ### for specific package(s)
        $ bash setup $package1 $package2 ...
        ```
        
    4. For other tasks for packages, or instructions, please read the documentations in `/scripts/doc`
    

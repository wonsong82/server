#commands
vagrant up
vagrant halt
vagrant plugin install
vagrant ssh-config


#packaging
Before packaging, add this key to .ssh/authorized_keys

insecure public key:
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key

src:
https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub

vagrant package --base "Name_of_Vbox" --output "out.box"
vagrant box add "Name_of_New_Box" [path]



#how to move vbox
remove .vagrant folder
vagrant up to start a new VM
vagrant halt to turn off the VM after boot

Open ~/.VirtualBox/VirtualBox.xml and find hdd uuid and copy uuid
Close VM, 
then override image.vmdk,
cd C:\Program Files\Oracle\VirtualBox
VboxManage internalcommands sethduuid imagefilepath uuid



#fix authentication problem
vagrant up and exit out when retrying
vagrant ssh-config to see private_key path
ssh-keygen -y -f private_key_path to find out public key of the private key
copy public key
vagrant ssh  to ssh into the server
paste the key into ~/.ssh/authorized_keys











SSL:

login ssh
/etc/ssl
mkdir sitename

openssl req -new -newkey rsa:2048 -nodes -keyout yourdoman.key -out yourdomain.csr




�ȳ��ϼ���. ���� ���޹ޱ�δ� SSL �¾��ϴ� ���� ���͵帱������ �˰��ֽ��ϴ�.

���� �̹� Certificate Ű�� �����ߴ��� �ñ��մϴ�. SSL �� ����ϱ����� crt ���ϰ� key ���� �׸��� ca-bundle ������ �ʿ��ѵ� eggbon���� SSL ����ϸ� �޴� �� 3���� ������ �ִ��� ���ð��. ���� cert �� ���� �����̴ٸ� SSL ����ϴ� ȸ�翡���� ssls.com ���� �� �������̸� 8�ҿ� �����ϽǼ��ֽ��ϴ�.

���� key ���ϰ� csr ������ ���弼��.
�͹̳ο��� 
openssl req -new -newkey rsa:2048 -nodes -keyout yourdoman.key -out yourdomain.csr

�׷� ssls ���� SSL �� �����Ͻð� �������� ����Ͻô� ������ csr �� ������ �մϴ�. csr ������ ī���ؼ� �������� ������ ���ʽ� verify �Ͻø� key �� ���� �����Ǽ� �����ǰ̴ϴ�.

�׷� aws �� ���� hosting �� �ش� site config �� ���� ssl �� ����Ͻø� �˴ϴ�. 
������ apache ���

/etc/apache2/sites-enabled/SITE.conf ��

<VirtualHost *:80> �ܿ� 
<VirtualHost *:443> ���� �ϳ� �� ����ð�

SSLEngine on
SSLCertificateFile /etc/ssl/domain/domain.crt
SSLCertificateKeyFile /etc/ssl/domain/domain.key
SSLCertificateChainFile /etc/ssl/domain/domain.ca-bundle

�̷��� �Ͻø� �˴ϴ�.
